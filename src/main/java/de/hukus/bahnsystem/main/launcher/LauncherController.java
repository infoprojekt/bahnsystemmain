/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.launcher;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.hukus.bahnsystem.main.auth.User;
import de.hukus.bahnsystem.main.manage.ManageViewPane;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import de.hukus.bahnsystem.main.user.UserViewPane;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author dbrosch und Fritz.Brandhuber
 */
public class LauncherController implements Initializable {

    private File f;
    private boolean ok = false;

    @FXML
    private TabPane root;

    @FXML
    private Tab startTab;

    @FXML
    private Tab loginTab;

    @FXML
    private Tab updateTab;

    @FXML
    private Tab errorTab;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private CheckBox save;

    @FXML
    private TextArea errorText;

    @FXML
    private ChoiceBox errorType;

    @FXML
    private TextField errorMail;

    @FXML
    private Label errorResult;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        root.getTabs().clear();
        root.getTabs().add(loginTab);
        root.getTabs().add(errorTab);
        errorResult.setText("");
        errorType.getItems().add("Fehler");
        errorType.getItems().add("Bedienung");
        errorType.getItems().add("Sonstiges");
        f = new File(System.getProperty("user.dir") + System.lineSeparator() + "BahnSystem" + System.lineSeparator() + "userdata.json");
        try {
            JsonObject o = new JsonParser().parse(new FileReader(f)).getAsJsonObject();
            String username = o.get("username").getAsString();
            String passhash = o.get("password").getAsString();
            User.user = new User(username, passhash, false);
            User.user.auth();
            if (User.user.isAuthed()) {
                root.getTabs().remove(loginTab);
                root.getTabs().add(0, startTab);
                root.getSelectionModel().selectFirst();
            } else {
                return;
            }
        } catch (Exception ex) {
            Logger.getLogger("BaSRV").log(Level.SEVERE, "Failed to quick-login.");
        }

    }

    @FXML
    public void onLoginClick(ActionEvent e) {
        if (!username.getText().isEmpty() && !password.getText().isEmpty()) {
            User.user = new User(username.getText(), password.getText(), true);
            //System.out.println("hash: " + u.getPassHash());
            User.user.auth();
            if (User.user.isAuthed()) {
                root.getTabs().remove(loginTab);
                root.getTabs().add(0, startTab);
                root.getSelectionModel().selectFirst();
                if (save.isSelected()) {
                    try {
                        if (f.exists()) {
                            f.delete();
                        }
                        f.createNewFile();
                        FileWriter w = new FileWriter(f);
                        JsonObject o = new JsonObject();
                        o.addProperty("username", User.user.getUsername());
                        o.addProperty("password", User.user.getPassHash());
                        w.write(o.toString());
                        w.close();
                    } catch (IOException ex) {
                        Logger.getLogger("BaSRV").log(Level.SEVERE, null, ex);
                    }
                }
            } else {
                System.out.println("Auth failed!");
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Login failed");
                a.setHeaderText("Der Benutzername oder das Passwort waren falsch.");
                a.showAndWait();
            }
        }
    }

    @FXML
    public void onLogoutClick(ActionEvent e) {
        User.user = null;
        f.delete();
        root.getTabs().clear();
        root.getTabs().add(loginTab);
        root.getTabs().add(errorTab);
    }

    @FXML
    public void onUserRequest(ActionEvent e) {
        UserViewPane userRoot = new UserViewPane(1000, 600);
        Scene userScene = new Scene(userRoot);
        userScene.getStylesheets().add("/styles/Styles.css");
        Stage userStage = new Stage();
        userStage.setTitle("GVV - Großdorfener Verkehrsverbund");
        userStage.setScene(userScene);
        userStage.setMaximized(true);
        userStage.show();
    }

    @FXML
    public void onManageRequest(ActionEvent e) {
        if (User.user.isAuthed() && User.user.getLevel() >= 80) {
            ManageViewPane managePane = new ManageViewPane(1000, 600);
            Scene manageScene = new Scene(managePane);
            Stage manageStage = new Stage();
            manageStage.setScene(manageScene);
            manageStage.setMaximized(true);
            manageStage.show();
        }
    }

    @FXML
    public void onTicketRequest(ActionEvent e) {
        if (User.user.isAuthed() && User.user.getLevel() >= 60) {
            try {
                AnchorPane ticketPane = FXMLLoader.load(getClass().getResource("/fxml/TicketCheckDesign.fxml"));
                Scene ticketScene = new Scene(ticketPane);
                Stage ticketStage = new Stage();
                ticketStage.setScene(ticketScene);
                ticketStage.show();
            } catch (IOException ex) {
                Logger.getLogger(LauncherController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    public void onErrorSubmit(ActionEvent e) {
        JsonObject o = new ServerRequest("report", new HashMap<String, String>() {
            {
                put("type", (String) errorType.getSelectionModel().getSelectedItem());
                put("email", errorMail.getText());
            }
        }, errorText.getText()).getAsJsonObject();
        if (o.get("result").getAsString().equalsIgnoreCase("success")) {
            errorResult.setText("Vielen Dank! Ihr Report wurde erfolgreich übermittelt.");
        } else {
            errorResult.setText("Leider konnte Ihr Report nicht korrekt übermittelt werden. Sie können jedoch gerne eine E-Mail an support@betafase.com schreiben, um schnelle Hilfe zu erhalten.");
        }
    }

}
