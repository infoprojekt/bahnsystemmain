/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.user;

import de.hukus.bahnsystem.main.map.Station;
import de.hukus.bahnsystem.main.map.StationView;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author dbrosch
 */
public class UserViewPane extends AnchorPane {

    public static UserViewPane instance;
    StationView v;
    AnchorPane r;
    MapInteractionHandler handler;

    public UserViewPane(double x, double y) {
        instance = this;
        setPrefHeight(1920);
        setPrefWidth(1080);
        setWidth(x);
        setHeight(y);

        v = new StationView();
        getChildren().add(v);
        handler = (Station s) -> {
        };
        try {
            r = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(UserViewPane.class.getName()).log(Level.SEVERE, null, ex);
        }
        getChildren().add(r);
        for (Station s : v.getStation()) {
            s.enableSelecting(handler);
        }
        load();
        widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            setWidth((double) newValue);
            load();
        });
        heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            setHeight((double) newValue);
            load();
        });
    }

    public void load() {
        System.out.println("Update in UserViewPane: " + getWidth() + " " + getHeight());
        if (getWidth() < 400 || getHeight() < 400) {
            //System.out.println("window is too small: " + getWidth() + " " + getHeight());
            v.setVisible(false);
            setLeftAnchor(r, 0.0);
        } else {
            if (!v.isVisible()) {
                v.setVisible(true);
            }
            loadMap();
        }
        setRightAnchor(r, 0.0);
        setTopAnchor(r, 0.0);
        setBottomAnchor(r, 0.0);
        //r.setPrefSize(getWidth() * 0.25 - 40, getHeight() - 40);
    }

    public void loadMap() {
        v.setLayoutX(20);
        v.setLayoutY(20);
        v.setWidth(getWidth() * 0.60);
        v.setHeight(getHeight() - 40);
        v.refreshCoords();
        setLeftAnchor(r, 0.6 * getWidth() + 40);
    }

}
