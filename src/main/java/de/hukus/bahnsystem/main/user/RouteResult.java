/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.user;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.map.Connection;
import de.hukus.bahnsystem.main.map.Highlightable;
import de.hukus.bahnsystem.main.map.Station;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Optional;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextBoundsType;

/**
 *
 * @author dbrosch
 */
public class RouteResult extends BorderPane {

    JsonObject result;
    final JsonArray zwischenhalt;
    final JsonArray line_type;
    final int start;
    final int end;
    Text t_left;
    Text t_right;
    JsonArray tickets;
    final int length;
    private int index;
    private final LinkedList<Highlightable> high;
    private final RouteController root;

    /**
     * Erzeugt ein neues Routenergebnis
     *
     * @param result JsonObject des Ergebnis für die Routenabfrage
     * @param root Gibt den übergeordneten Route-Controller an (I am Root!)
     * @param index Gibt den Index des RouteResults
     */
    public RouteResult(JsonObject result, RouteController root, int index) {
        this.high = new LinkedList<>();
        this.line_type = result.get("line_types").getAsJsonArray();
        this.zwischenhalt = result.get("route").getAsJsonArray();
        this.result = result;
        this.start = result.get("start").getAsInt();
        this.end = result.get("dest").getAsInt();
        this.length = result.get("length").getAsInt();
        this.root = root;
        this.index = index;
        this.tickets = result.get("alltickets").getAsJsonArray();
        loadData();
    }

    /**
     * Lädt die Daten aus dem JsonObject, das im Konstruktur übergeben wurde und
     * liest diesen aus
     */
    public void loadData() {
        System.out.println("Processing " + result.toString());

        String zwischenhalte = "";
        String line_types = "";
        for (int i3 = 0; i3 < line_type.size(); i3++) {
            if (i3 > 0) {
                line_types += ", ";
            }
            line_types += line_type.get(i3).getAsString();
        }
        for (int i2 = 0; i2 < zwischenhalt.size(); i2++) {
            JsonObject z = zwischenhalt.get(i2).getAsJsonObject();
            if (i2 == 2 && zwischenhalt.size() > 3) {
                zwischenhalte = zwischenhalte.substring(0, zwischenhalte.length() - 3) + " & " + (zwischenhalt.size() - i2 - 1) + " weitere. - ";
                break;
            }
            if (i2 < zwischenhalt.size() - 1) {
                zwischenhalte = zwischenhalte + z.get("end").getAsString() + " - ";
            }
        }
        System.out.println("Result: " + line_types + " über" + zwischenhalte + " (Umstiege: " + result.get("interchanges").getAsString() + ")");
        String text_links = line_types + System.lineSeparator();
        if (!zwischenhalte.equals("")) {
            zwischenhalte = zwischenhalte.substring(0, zwischenhalte.length() - 3);
            text_links += "über " + zwischenhalte + " (Umstiege: " + result.get("interchanges").getAsString() + ")" + System.lineSeparator();
        }
        text_links += "Dauer: " + ConvertMinutesToReadableTime(length);

        DecimalFormat df = new DecimalFormat("####0.00");
        t_left = new Text(text_links);
        t_right = new Text(df.format(result.get("bestticket").getAsDouble() / 100).replace(".", ",") + "€");

        setStyle("-fx-padding: 10px;");
        setLeft(t_left);
        setRight(t_right);
    }

    /**
     * Bringt das Ergebnis zurück in den Urzustand, wenn es nicht mehr
     * ausgewählt ist
     */
    public void deselect() {
        Platform.runLater(() -> {
            // Entferne sämtliche Elemente aus dem Ergebnis
            if (getLeft() != null) {
                getChildren().remove(getLeft());
            }
            if (getRight() != null) {
                getChildren().remove(getRight());
            }
            // Stelle ursprüngliche Elemente wieder her
            setLeft(t_left);
            setRight(t_right);
            // Das Routen-Highlighting zurücksetzen
            for (Highlightable h : high) {
                h.setHighlighted(false);
            }
            high.clear();
        });
    }

    /**
     * Bei Auswahl des Ergebnis fokussiert diese Methode alles auf die
     * ausgewählte Route
     */
    public void select() {
        // Erstellt die Box, die die Zwischenhaltinformationen enthält
        final VBox h = new VBox(15);
        for (JsonElement e1 : zwischenhalt) {
            // Erstellt die Objekte für die detailliertere Ticket-Anzeige
            JsonObject o = e1.getAsJsonObject();
            HBox info = new HBox(10);
            info.setAlignment(Pos.CENTER_LEFT);
            Pane visible = new Pane();
            visible.getChildren().add(new Rectangle(40, 40, Color.web(o.get("line_color").getAsString())));
            String url;
            //Alle Bilder sind 24x24
            //Credit: http://www.flaticon.com/packs/ways-of-transport
            switch (o.get("line_type").getAsString()) {
                case "Zug":
                    url = "images/locomotive.png";
                    break;
                case "U-Bahn":
                    url = "images/underground.png";
                    break;
                case "S-Bahn":
                    url = "images/underground.png";
                    break;
                case "Ruderboot":
                    url = "images/boat.png";
                    break;
                case "Bus":
                    url = "images/public-bus.png";
                    break;
                default:
                    url = "images/locomotive.png";
                    break;
            }
            ImageView v = new ImageView(new Image(url, 24, 24, false, true, true));
            v.setX(8);
            v.setY(3);
            visible.getChildren().add(v);
            Text t = new Text(o.get("line_name").getAsString());
            t.setX(0);
            t.setY(32);
            t.setBoundsType(TextBoundsType.VISUAL);
            t.setTextOrigin(VPos.TOP);
            t.setTextAlignment(TextAlignment.CENTER);
            t.setFont(new Font(10));
            visible.getChildren().add(t);
            info.getChildren().add(visible);
            Text data = new Text(
                    o.get("start").getAsString() + " " + System.lineSeparator()
                    + "bis " + System.lineSeparator()
                    + o.get("end").getAsString()
            );
            data.setWrappingWidth(400);
            data.setBoundsType(TextBoundsType.VISUAL);
            data.setTextAlignment(TextAlignment.LEFT);
            data.setTextOrigin(VPos.CENTER);
            info.getChildren().add(data);
            h.getChildren().add(info);
        }
        Text gesamt = new Text("Gesamtdauer: " + ConvertMinutesToReadableTime(result.get("length").getAsInt()));
        h.getChildren().add(gesamt);
        BorderPane pay = new BorderPane();
        Label l = new Label(t_right.getText());
        pay.setAlignment(l, Pos.TOP_RIGHT);
        l.setTextAlignment(TextAlignment.RIGHT);
        l.setTextFill(Color.BLACK);
        pay.setTop(l);
        // Button hinzufügen, um ein Ticket zu kaufen
        Button buy = new Button("Tickets anzeigen");
        pay.setBottom(buy);
        // Bei klicken des "Ticket kaufen"-Buttons wird eine Übersicht aller verfügbaren Tickets angezeigt
        buy.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                // Liste, die die Elemente für die Ticket-Liste enthält
                ObservableList<BorderPane> items = FXCollections.observableArrayList();
                root.getTicketList().setVisible(true);
                root.backButton.setVisible(true);
                for (int i = 0; i < tickets.size(); i++) {
                    // Ticket-Variable abholen
                    JsonObject ticket = tickets.get(i).getAsJsonObject();

                    BorderPane ticket_box = new BorderPane();

                    DecimalFormat df = new DecimalFormat("####0.00");

                    String ticket_name = "Name: " + ticket.get("name").getAsString();
                    String ticket_preis = "Preis: " + df.format(ticket.get("price").getAsDouble() / 100).replace(".", ",") + "€";

                    String ticket_dauer = "";
                    long dauer = ticket.get("dauer").getAsLong();
                    if (dauer >= 1000) {
                        Date d = new Date(dauer);
                        if (d.getMonth() == 1) {
                            ticket_dauer = "Monatskarte";
                        } else if (d.getDate() == 7) {
                            ticket_dauer = "Wochenkarte";
                        } else {
                            ticket_dauer = "Gültig für " + ConvertMinutesToReadableTime(ticket.get("dauer").getAsLong() / 60000);
                        }
                    } else {
                        ticket_dauer = "Einzelfahrt";
                    }

                    // Reihe Elemente aneinander, für die das Ticket gültig ist
                    String[] ticket_linien = new String[5];
                    if (ticket.get("sbahn").getAsString().equals("true")) {
                        ticket_linien[0] = "S-Bahn";
                    }
                    if (ticket.get("ubahn").getAsString().equals("true")) {
                        ticket_linien[1] = "U-Bahn";
                    }
                    if (ticket.get("zug").getAsString().equals("true")) {
                        ticket_linien[2] = "Zug";
                    }
                    if (ticket.get("bus").getAsString().equals("true")) {
                        ticket_linien[3] = "Bus";
                    }
                    if (ticket.get("ruderboot").getAsString().equals("true")) {
                        ticket_linien[4] = "Ruderboot";
                    }

                    // Zeige Gültigkeit an
                    String ticket_linien_zsm = "Gültig für: " + arrayZusammenfuehren(ticket_linien);

                    Text ticket_text = new Text(ticket_name + System.lineSeparator() + ticket_preis + System.lineSeparator() + ticket_dauer + System.lineSeparator() + ticket_linien_zsm);
                    ticket_box.setLeft(ticket_text);
                    Button buy_btn = new Button("Kaufen");
                    ticket_box.setRight(buy_btn);

                    // Kümmert sich um die Dialogfenster, falls Kunde sich für ein Ticket entschieden hat
                    buy_btn.onMouseClickedProperty().set(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            // Auswahlfenster SMS oder E-Mail
                            Alert alert = new Alert(AlertType.CONFIRMATION);
                            alert.setTitle("Ticketkauf");
                            alert.setHeaderText("Ticketkauf");
                            alert.setContentText("Bitte wählen Sie, wie Sie Ihr Ticket erhalten möchten.");

                            ButtonType buttonTypeOne = new ButtonType("SMS (T-Mobile)");
                            ButtonType buttonTypeTwo = new ButtonType("E-Mail");
                            ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

                            alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

                            Optional<ButtonType> ticket_type = alert.showAndWait();
                            if (ticket_type.get() == buttonTypeOne) {
                                // Dialog zum Abholen der Handynummer
                                TextInputDialog dialog = new TextInputDialog("Handynummer");
                                dialog.setTitle("Ticketkauf");
                                dialog.setHeaderText("Ticketkauf");
                                dialog.setContentText("Handynummer:");

                                Optional<String> sms = dialog.showAndWait();
                                if (sms.isPresent()) {
                                    // Sende Handynummer an den Webserver
                                    ServerRequest sr = new ServerRequest("ticket/buy", new HashMap<String, String>() {
                                        {
                                            put("type", Integer.toString(10));
                                            put("time", ticket.get("dauer").getAsString());
                                            put("phone", sms.get());
                                        }
                                    });
                                    // Prüfe Antwort des Webservers
                                    if (sr.getAsJsonObject().get("result").getAsString().equals("success")) {
                                        // Bei Erfolg der Anfrage den Ticketkauf bestätigen
                                        Alert confirm_ticket = new Alert(AlertType.INFORMATION);
                                        confirm_ticket.setTitle("Ticketkauf");
                                        confirm_ticket.setHeaderText("Ticketkauf");
                                        confirm_ticket.setContentText("Ihre Ticketinformationen wurden an die angegebene Handynummer gesendet. Wir wünschen eine gute Fahrt!");

                                        confirm_ticket.showAndWait();
                                    } else {
                                        // Falls ein Fehler auftreten sollte diesen ausgeben
                                        Alert error_ticket = new Alert(AlertType.ERROR);
                                        error_ticket.setTitle("Ticketkauf");
                                        error_ticket.setHeaderText("Ticketkauf");
                                        error_ticket.setContentText("Beim Kauf Ihres Tickets trat ein Fehler auf. Bitte wenden Sie sich an unsere Servicezentrale.");

                                        error_ticket.showAndWait();
                                    }
                                } else {
                                    // Bei Abbruch des Ticketskauf den Kunden darüber informieren
                                    Alert cancel_ticket = new Alert(AlertType.WARNING);
                                    cancel_ticket.setTitle("Ticketkauf");
                                    cancel_ticket.setHeaderText("Ticketkauf");
                                    cancel_ticket.setContentText("Der Ticketkauf wird nun abgebrochen und Ihre Informationen werden verworfen.");

                                    cancel_ticket.showAndWait();
                                }
                            } else if (ticket_type.get() == buttonTypeTwo) {
                                // Falls E-Mail ausgewählt Dialog zum Abholen der E-Mail
                                TextInputDialog dialog = new TextInputDialog("E-Mail");
                                dialog.setTitle("Ticketkauf");
                                dialog.setHeaderText("Ticketkauf");
                                dialog.setContentText("E-Mailadresse:");

                                // Traditional way to get the response value.
                                Optional<String> email = dialog.showAndWait();
                                if (email.isPresent()) {
                                    // Ergebnis an Webserver senden
                                    ServerRequest sr = new ServerRequest("ticket/buy", new HashMap<String, String>() {
                                        {
                                            put("type", Integer.toString(10));
                                            put("time", ticket.get("dauer").getAsString());
                                            put("email", email.get());
                                        }
                                    });
                                    if (sr.getAsJsonObject().get("result").getAsString().equals("success")) {
                                        // Falls Ergebnis erfolgreich gesendet wurde Kunden informieren
                                        Alert confirm_ticket = new Alert(AlertType.INFORMATION);
                                        confirm_ticket.setTitle("Ticketkauf");
                                        confirm_ticket.setHeaderText("Ticketkauf");
                                        confirm_ticket.setContentText("Ihre Ticketinformationen wurden an die angegebene E-Mailadresse gesendet. Wir wünschen eine gute Fahrt!");

                                        confirm_ticket.showAndWait();
                                    } else {
                                        // Falls ein Fehler auftrat Kunden informieren
                                        Alert error_ticket = new Alert(AlertType.ERROR);
                                        error_ticket.setTitle("Ticketkauf");
                                        error_ticket.setHeaderText("Ticketkauf");
                                        error_ticket.setContentText("Beim Kauf Ihres Tickets trat ein Fehler auf. Bitte wenden Sie sich an unsere Servicezentrale.");

                                        error_ticket.showAndWait();
                                    }
                                } else {
                                    // Bei Abbruch Kunden informieren
                                    Alert cancel_ticket = new Alert(AlertType.WARNING);
                                    cancel_ticket.setTitle("Ticketkauf");
                                    cancel_ticket.setHeaderText("Ticketkauf");
                                    cancel_ticket.setContentText("Der Ticketkauf wird nun abgebrochen und Ihre Informationen werden verworfen.");

                                    cancel_ticket.showAndWait();
                                }
                            } else {
                                // Bei Abbruch nach Schritt 1 Kunden informieren
                                Alert cancel_ticket = new Alert(AlertType.WARNING);
                                cancel_ticket.setTitle("Ticketkauf");
                                cancel_ticket.setHeaderText("Ticketkauf");
                                cancel_ticket.setContentText("Der Ticketkauf wird nun abgebrochen und Ihre Informationen werden verworfen.");

                                cancel_ticket.showAndWait();
                            }
                        }
                    });

                    // Die Ticket-Möglichkeit in die ListView hinzufügen
                    items.add(ticket_box);
                }

                // ListView mit den Items der Liste befüllen
                root.getTicketList().setItems(items);
            }
        });
        Platform.runLater(() -> {
            // Routenergebnis anzeigen
            getChildren().remove(t_left);
            setLeft(h);
            getChildren().remove(t_right);
            setRight(pay);
            // Route anhand von gehighlighteten Streckenabschnitten anzeigen
            JsonArray highlighted = result.get("highlight").getAsJsonArray();
            for (JsonElement e2 : highlighted) {
                JsonObject toH = e2.getAsJsonObject();
                if (toH.get("type").getAsString().equalsIgnoreCase("connection")) {
                    Connection c = UserViewPane.instance.v.getConnection()[toH.get("id").getAsInt()];
                    c.setHighlighted(true);
                    c.setHighlightType(Station.HighlightType.ROUTE);
                    high.add(c);
                } else if (toH.get("type").getAsString().equalsIgnoreCase("station")) {
                    Station s = UserViewPane.instance.v.getStation()[toH.get("id").getAsInt()];
                    s.setHighlighted(true);
                    s.setHighlightType(Station.HighlightType.ROUTE);
                    high.add(s);
                }
            }
            // Ergebnis in Karte anzeigen
            root.updateFrom(start);
            root.updateTo(end);
        });
    }

    /**
     * Konvertiert eine Zeit in Minuten in eine besser lesbare Zeit in Format
     * HH:MM
     *
     * @param minutes Minuten, die umgewandelt werden sollen
     * @return String, der lesbare Zeit im Format HH:MM enthält
     */
    private static String ConvertMinutesToReadableTime(long minutes_input) {
        long minutes = minutes_input % 60;
        long hours = (minutes_input - minutes) / 60;
        long buffer = hours;
        hours = hours % 24;
        long days = (buffer - hours) / 24;
        StringBuilder result = new StringBuilder();
        if (days > 1) {
            result.append(days).append(" Tage ");
        } else if (days == 1) {
            result.append(days).append(" Tag ");
        }
        if (hours > 0 || minutes > 0) {
            result.append(hours).append(":").append(minutes < 10 ? 0 : "").append(minutes).append(" h");
        }
        return result.toString();
    }

    /**
     * Führt einen Array aus Strings zusammen
     *
     * @param arr Array aus Strings
     * @return String aus den mit Komma getrennten Elementen des Arrays
     */
    private String arrayZusammenfuehren(String[] arr) {
        String zsm = "";
        boolean first = true;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null) {
                if (first == false) {
                    zsm += ", ";
                }
                zsm += arr[i];
                first = false;
            }
        }
        return zsm;
    }

}
