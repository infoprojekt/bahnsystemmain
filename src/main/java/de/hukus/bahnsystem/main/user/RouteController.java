/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.user;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.map.Highlightable;
import de.hukus.bahnsystem.main.map.Station;
import de.hukus.bahnsystem.main.requests.RouteRequest;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 *
 * @author dbrosch
 */
public class RouteController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private Label title;

    @FXML
    private Label subtitle;

    @FXML
    private TextField location_from;

    @FXML
    private TextField location_to;

    @FXML
    private ListView results;

    @FXML
    private Label starterror;

    @FXML
    private Label enderror;

    @FXML
    private CheckBox select_ubahn;

    @FXML
    private CheckBox select_sbahn;

    @FXML
    private CheckBox select_bus;

    @FXML
    private CheckBox select_zug;

    @FXML
    private CheckBox select_ruderboot;

    @FXML
    private ListView<BorderPane> ticket_list;

    @FXML
    public Button backButton;

    private RouteResult rr = null;

    private int from_id = -1;
    private int to_id = -1;
    private LinkedList<Highlightable> high = new LinkedList<>();

    public void updateFrom(int newid) {
        if (from_id >= 0) {
            Station s = UserViewPane.instance.v.getStation()[from_id];
            s.setHighlighted(false);
        }
        from_id = newid;
        if (newid >= 0) {
            Station s = UserViewPane.instance.v.getStation()[from_id];
            s.setHighlighted(true);
            s.setHighlightType(Station.HighlightType.GREEN);
            if (from_id == to_id) {
                starterror.setText("Das ist nicht besonders weit!");
            } else {
                starterror.setText("");
            }
        } else {
            starterror.setText("Diese Station existiert nicht.");
        }
    }

    public void updateTo(int newid) {
        if (to_id >= 0) {
            Station s = UserViewPane.instance.v.getStation()[to_id];
            s.setHighlighted(false);
        }
        to_id = newid;
        if (newid >= 0) {
            Station s = UserViewPane.instance.v.getStation()[to_id];
            s.setHighlighted(true);
            s.setHighlightType(Station.HighlightType.RED);
            if (from_id == to_id) {
                enderror.setText("Das ist nicht besonders weit!");
            } else {
                enderror.setText("");
            }
        } else {
            enderror.setText("Diese Station existiert nicht.");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        UserViewPane.instance.handler = new MapInteractionHandler() {
            @Override
            public void onSelect(Station s) {
                if (location_from.isFocused()) {
                    s.setHighlighted(true);
                    s.setHighlightType(Station.HighlightType.GREEN);
                    location_from.setText(s.getName());
                    updateFrom(s.getID());
                } else if (location_to.isFocused()) {
                    s.setHighlighted(true);
                    s.setHighlightType(Station.HighlightType.RED);
                    location_to.setText(s.getName());
                    updateTo(s.getID());
                }
            }
        };
        backButton.setOnAction((ActionEvent event) -> {
            ticket_list.setVisible(false);
            backButton.setVisible(false);
        });
        //results.setBorder(new Border(new BorderStroke(null, BorderStrokeStyle.SOLID, new CornerRadii(2), BorderStroke.MEDIUM)));
        results.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        results.setEditable(false);
        root.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                int i = newValue.intValue();
                if (i < 380) {
                    title.setText("GVV");
                } else {
                    title.setText("GVV - We care about you!");
                }
                if (i < 300) {
                    subtitle.setText("gvv.de");
                } else {
                    subtitle.setText("Großdorfener Verkehrsverbund");
                }
            }
        });
        updateFrom(UserViewPane.instance.v.getStationIDfromName(location_from.getText()));
        updateTo(UserViewPane.instance.v.getStationIDfromName(location_to.getText()));
    }

    @FXML
    public void handleTextType(KeyEvent e) {
        updateFrom(UserViewPane.instance.v.getStationIDfromName(location_from.getText()));
        updateTo(UserViewPane.instance.v.getStationIDfromName(location_to.getText()));
    }

    @FXML
    public void handleSearchAction(ActionEvent e) {
        if (from_id < 0 || to_id < 0) {
            return;
        }
        final String from = location_from.getText();
        final String to = location_to.getText();
        final boolean ubahn = select_ubahn.isSelected();
        final boolean sbahn = select_sbahn.isSelected();
        final boolean bus = select_bus.isSelected();
        final boolean ruderboot = select_ruderboot.isSelected();
        final boolean zug = select_zug.isSelected();
        results.getItems().clear();
        HBox loading = new HBox(5);
        loading.setFillHeight(true);
        ImageView icon = new ImageView("images/loading.gif");
        loading.getChildren().add(icon);
        loading.setAlignment(Pos.CENTER);
        results.getItems().add(loading);
        results.getSelectionModel().selectedItemProperty().addListener((ObservableValue observable, Object oldValue, Object newValue) -> {
            if (rr != null) {
                ((RouteResult) rr).deselect();
            }
            rr = (RouteResult) newValue;
            if (rr != null) {
                rr.select();
//                results.scrollTo(rr);
            }
        });
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    if (rr != null) {
                        rr.deselect();
                        rr = null;
                    }
                    RouteRequest rq = new RouteRequest(from, to, sbahn, ubahn, bus, zug, ruderboot);
                    System.out.println("Wert vom Server abrufen...");
                    Platform.runLater(() -> {
                        results.getItems().remove(0);
                    });
                    JsonObject route = rq.getAsJsonObject();
                    if (route.get("result").getAsString().equalsIgnoreCase("success")) {
                        System.out.println("Erfolgreich abgerufen.");
                        JsonArray routes = route.get("routes").getAsJsonArray();
                        int index = 0;
                        for (JsonElement e0 : routes) {
                            RouteResult rs = new RouteResult(e0.getAsJsonObject(), RouteController.this, index);
                            boolean ok = true;
                            //Removes deselected routes.
                            for (JsonElement e : rs.line_type) {
                                String s = e.getAsString();
                                if (!sbahn && s.equalsIgnoreCase("S-Bahn") || !ubahn && s.equalsIgnoreCase("U-Bahn") || !bus && s.equalsIgnoreCase("Bus") || !zug && s.equalsIgnoreCase("Zug") || !ruderboot && s.equalsIgnoreCase("Ruderboot")) {
                                    System.out.println("Removed suspicious route.");
                                    ok = false;
                                    break;
                                }
                            }
                            if (ok) {
                                index++;
                                Platform.runLater(() -> {
                                    results.getItems().add(rs);
                                });
                            }
                        }
                    } else {
                        HBox error = new HBox(5);
                        ImageView errorImage = new ImageView("images/notification_error.png");
                        error.getChildren().add(errorImage);
                        Text errorTitle = new Text("Anzeigen der Routen fehlgeschlagen!" + System.lineSeparator());
                        errorTitle.setStyle("-fx-font-weight: bold");
                        Text errorDescription = new Text();
                        errorDescription.setWrappingWidth(10);
                        errorDescription.setText(route.get("message").getAsString());
                        TextFlow f = new TextFlow(errorTitle, errorDescription);
                        f.setMaxWidth(results.widthProperty().doubleValue() - 70);
                        error.getChildren().add(f);
                        Platform.runLater(() -> {
                            results.getItems().clear();
                            results.getItems().add(error);
                        });
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        };
        new Thread(task).start();

    }

    public ListView getTicketList() {
        return ticket_list;
    }
}
