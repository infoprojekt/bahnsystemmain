/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.user;

import de.hukus.bahnsystem.main.map.Station;

/**
 *
 * @author dbrosch
 */
public interface MapInteractionHandler {

    public void onSelect(Station s);
}
