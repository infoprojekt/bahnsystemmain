/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.auth;

import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dbrosch
 */
public class User {

    public static User user;

    private String username;
    private String passHash;
    private String email;
    private int level;
    private boolean authed;

    public User(String username, String pass, boolean hash) {
        try {
            this.username = username;
            if (hash) {
                this.passHash = sha256(pass);
            } else {
                this.passHash = pass;
            }
        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    private static String sha256(String base) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(base.getBytes("UTF-8"));
            StringBuilder hexString = new StringBuilder();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public void auth() {
        ServerRequest r = new ServerRequest("auth");
        JsonObject o = r.getAsJsonObject();
        if (authed = o.get("result").getAsString().equalsIgnoreCase("success")) {
            level = 100;
            email = "nope";
            //TODO add dat info here
        }
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassHash() {
        return passHash;
    }

    public int getLevel() {
        return level;
    }

    public boolean isAuthed() {
        return authed;
    }

}
