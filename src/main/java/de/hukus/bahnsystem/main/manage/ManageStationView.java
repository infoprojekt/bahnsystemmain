/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.manage;

import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.map.Connection;
import de.hukus.bahnsystem.main.map.Coords2D;
import de.hukus.bahnsystem.main.map.Line;
import de.hukus.bahnsystem.main.map.Station;
import de.hukus.bahnsystem.main.map.StationView;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author dbrosch
 */
public class ManageStationView extends StationView {

    public HashMap<String, Change> changes = new HashMap<>();

    public static NewCon addCon = null;

    private ManageViewPane root;

    public ManageStationView(ManageViewPane root) {
        this.root = root;
        ContextMenu menu = new ContextMenu();
        MenuItem add = new MenuItem("Station hinzufügen");
        menu.getItems().add(add);
        setOnMouseMoved(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (addCon != null) {
                    addCon.endXProperty().set(event.getX());
                    addCon.endYProperty().set(event.getY());
                }
            }
        });
        setOnMousePressed((MouseEvent event) -> {
            if (addCon != null) {
                return;
            }
            if (!menu.isShowing() && event.isSecondaryButtonDown()) {
                menu.show(ManageStationView.this, event.getScreenX(), event.getScreenY());
                add.setOnAction((ActionEvent event1) -> {
                    //Rufe Koordinaten des Rechtsklickpunktes ab
                    Coords2D c = getCoords(event.getX(), event.getY());
                    // Erstelle Dialog, um nach dem Namen zu Fragen
                    TextInputDialog dialog = new TextInputDialog("");
                    dialog.setTitle("Namenseingabe");
                    dialog.setHeaderText("Namenseingabe");
                    dialog.setContentText("Bitte gib den Namen der Station bei ( " + c.getX() + " | " + c.getY() + " ) ein: ");

                    // Rufe Ergebnis der Abfrage ab
                    Optional<String> result = dialog.showAndWait();
                    if (result.isPresent()) {
                        JsonObject jo = new ServerRequest("map/add_station", new HashMap<String, String>() {
                            {
                                put("name", result.get());
                                put("x", c.getX().toString());
                                put("y", c.getY().toString());
                            }
                        }).getAsJsonObject();
                        if (jo.get("result").getAsString().equals("success")) {
                            Alert alert = new Alert(Alert.AlertType.INFORMATION);
                            alert.setTitle("Hinweis");
                            alert.setHeaderText("Ergebnis");
                            alert.setContentText("Die Station " + result.get() + " wurde hinzugefügt.");
                            alert.showAndWait();
                            loadData();
                            root.loadStationView();
                        }
                    }
                });
            } else if (menu.isShowing()) {
                menu.hide();
            }
        });
    }

    @Override
    public void loadData() {
        super.loadData();
        InfoUpdateHandler info = new InfoUpdateHandler() {
            @Override
            public void handleStationDragCompletion(Station s, Coords2D<Double, Double> previous) {
                if (!changes.containsKey("stationmove" + s.getID())) {
                    System.out.println("Updating move event!");
                    final double previousX = previous.getX();
                    final double previousY = previous.getY();
                    changes.put("stationmove" + s.getID(), (Change) () -> {
                        Coords2D<Integer, Integer> newC = getCoords(s.getLayoutX(), s.getLayoutY());
                        Coords2D<Integer, Integer> preC = getCoords(previous.getX(), previous.getY());
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
                        a.setTitle("Änderung einer Station");
                        a.setHeaderText("Die Station " + s.getName() + " wurde von ( " + preC.getX() + " | " + preC.getY() + " ) nach ( " + newC.getX() + " | " + newC.getY() + " ) verschoben. Möchten Sie dies speichern?");
                        a.getButtonTypes().clear();
                        a.getButtonTypes().addAll(ButtonType.NO, ButtonType.YES);
                        Optional<ButtonType> result = a.showAndWait();
                        if (result.isPresent() && result.get() == ButtonType.YES) {
                            ServerRequest r = new ServerRequest("map/add_station", new HashMap<String, String>() {
                                {
                                    put("name", s.getName());
                                    put("x", Integer.toString(newC.getX()));
                                    put("y", Integer.toString(newC.getY()));
                                }
                            });
                            JsonObject response = r.getAsJsonObject();
                            if (response.get("result").getAsString().equalsIgnoreCase("success")) {
                                loadData();
                                root.loadStationView();
                            } else {
                                s.setLayoutX(previousX);
                                s.setLayoutY(previousY);
                            }
                        } else {
                            s.setLayoutX(previousX);
                            s.setLayoutY(previousY);
                        }
                    });
                }
            }

            @Override
            public void handleConnectionAddCompletion(int start, int end) {

                // http://host01.betafase.com:1999/map/add_connection?start=0&end=5&length=7&line=1
                // -------------------------------
                // DIALOG: AUSWAHL DER LINIE
                // -------------------------------
                // Get connections
                List<Line> choices = new ArrayList<>();

                for (Line l : getLine()) {
                    if ((l.getStart() == -1 && l.getEnd() == -1) || (l.getEnd() == start) || (l.getStart() == start)) {
                        choices.add(l);
                    }
                }
                System.out.println("Size: " + getLine().length);

                try {
                    if (choices.size() > 0) {
                        ChoiceDialog<Line> dialog = new ChoiceDialog<>(choices.get(0), choices);
                        dialog.setTitle("Auswahldialog");
                        dialog.setHeaderText("Linienauswahl");
                        dialog.setContentText("Bitte wählen Sie die Linie, die die Verbindung befahren soll.");

                        // Traditional way to get the response value.
                        Optional<Line> result = dialog.showAndWait();
                        if (result.isPresent()) {
                            TextInputDialog input = new TextInputDialog(5 + "");
                            input.setTitle("Längeneingabe");
                            input.setHeaderText("Bitte geben Sie die Länge der Strecke ein:");
                            input.getEditor().textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                                try {
                                    int i = Integer.parseInt(newValue);
                                } catch (NumberFormatException ex) {
                                    input.getEditor().textProperty().setValue(oldValue);
                                }
                            });
                            Optional<String> lenght_R = input.showAndWait();
                            if (lenght_R.isPresent()) {
                                // if user doesnt cancel, continue with request
                                Line line = result.get();
                                final int line_id = line.getId();
                                final int length = Integer.parseInt(lenght_R.get());
                                // Sending request to server
                                ServerRequest sr = new ServerRequest("map/add_connection", new HashMap<String, String>() {
                                    {
                                        put("start", Integer.toString(start));
                                        put("end", Integer.toString(end));
                                        put("line", Integer.toString(line_id));
                                        put("length", Integer.toString(length));
                                    }
                                });
                                if (sr.getAsJsonObject().get("result").getAsString().equalsIgnoreCase("success")) {
                                    loadData();
                                    root.loadStationView();
                                    addCon = null;
                                    return;
                                }
                            }
                        }
                    } else {
                        Alert a = new Alert(Alert.AlertType.ERROR, "Es sind leider hier keine Linien verfügbar!", ButtonType.CLOSE);
                        a.showAndWait();
                    }
                    getStation()[end].setHighlighted(false);
                    getChildren().remove(addCon);
                    addCon = null;
                    System.out.println("Reached this!");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            @Override
            public void handleConnectionAddStart(NewCon c) {
                getChildren().add(c);
                addCon = c;
                c.toBack();
            }

            @Override
            public void handleStationRemove(Station s) {
                for (Connection c : getConnection()) {
                    if (c.getStartID() == s.getID() || c.getEndID() == s.getID()) {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setContentText("Die Station darf mit keiner Linie mehr verbunden sein!");
                        break;
                    }
                }
                ServerRequest r = new ServerRequest("map/remove_station", new HashMap<String, String>() {
                    {
                        put("id", Integer.toString(s.getID()));
                    }
                });
                JsonObject o = r.getAsJsonObject();
                if (o.get("result").getAsString().equalsIgnoreCase("success")) {
                    loadData();
                    root.loadStationView();
                } else {
                    Alert error = new Alert(Alert.AlertType.ERROR);
                    error.setContentText(o.has("message") ? o.get("message").getAsString() : o.get("description").getAsString());
                    error.showAndWait();
                }
            }

            @Override
            public void handleConnectionRemove(int id) {
                ServerRequest r = new ServerRequest("map/remove_connection", new HashMap<String, String>() {
                    {
                        put("id", Integer.toString(id));
                    }
                });
                JsonObject o = r.getAsJsonObject();
                if (o.get("result").getAsString().equalsIgnoreCase("success")) {
                    loadData();
                    root.loadStationView();
                } else {
                    Alert error = new Alert(Alert.AlertType.ERROR);
                    error.setContentText(o.has("message") ? o.get("message").getAsString() : o.get("description").getAsString());
                    error.showAndWait();
                }

            }

            @Override
            public boolean canRemove(Connection c) {
                Line l = c.getLine();
                return !(l.getEnd() == c.getEndID() || l.getStart() == c.getStartID() || l.getEnd() == c.getStartID() || l.getStart() == c.getEndID());
            }
        };
        for (Station s : stations) {
            s.enableEditing(info);
        }
        for (Connection c : connections) {
            c.enableEditing(info);
        }
    }

}
