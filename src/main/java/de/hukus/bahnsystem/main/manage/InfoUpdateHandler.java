/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.manage;

import de.hukus.bahnsystem.main.map.Connection;
import de.hukus.bahnsystem.main.map.Coords2D;
import de.hukus.bahnsystem.main.map.Station;

/**
 *
 * @author dbrosch
 */
public interface InfoUpdateHandler {

    public void handleStationDragCompletion(Station s, Coords2D<Double, Double> previous);
    
    public void handleStationRemove(Station s);
    
    public boolean canRemove(Connection c);
    
    public void handleConnectionAddCompletion(int start, int end);
    
    public void handleConnectionAddStart(NewCon c);
    
    public void handleConnectionRemove(int id);
    

}
