/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.manage;

import de.hukus.bahnsystem.main.map.Connection;
import de.hukus.bahnsystem.main.map.Line;
import de.hukus.bahnsystem.main.map.Station;
import javafx.scene.paint.Color;

/**
 *
 * @author dbrosch
 */
public class NewCon extends Connection {

    public NewCon(Station s) {
        super(-1, s.getID(), -1, -1, new Line(-1, "secret", "sonstwas", 0, 0, 0, "yellow"));
        this.startXProperty().bind(s.getCenterX());
        this.startYProperty().bind(s.getCenterY());
        this.setStroke(Color.rgb(202, 225, 255, 0.8));
        this.setHighlighted(true);
    }

}
