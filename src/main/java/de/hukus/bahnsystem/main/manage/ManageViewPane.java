/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.manage;

import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.map.Line;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author dbrosch
 */
public class ManageViewPane extends AnchorPane {

    ManageStationView m;

    public ManageViewPane(double x_size, double y_size) {
        setWidth(x_size);
        setHeight(y_size);
        MenuBar b = new MenuBar();
        b.setUseSystemMenuBar(true);
        b.setLayoutX(0);
        b.setLayoutY(0);

        Menu data = new Menu("Datei");
        Menu server = new Menu("Server");
        Menu line = new Menu("Linie");
        MenuItem editLines = new MenuItem("Linien anzeigen");
        editLines.setOnAction((ActionEvent event) -> {
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setTitle("Linien anzeigen");
            dialog.setHeaderText("Diese Linien existieren");
            dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
            ListView v = new ListView();
            for (Line l : m.getLine()) {
                VBox info = new VBox(5);
                info.setBackground(new Background(new BackgroundFill(Color.web(l.getColor()), CornerRadii.EMPTY, Insets.EMPTY)));
                info.getChildren().add(new Text(l.getName()));
                info.getChildren().add(new Text(l.getType()));
                v.getItems().add(info);
            }
            v.setMaxHeight(200);
            dialog.getDialogPane().setContent(v);
            dialog.showAndWait();
        });
        MenuItem clearLines = new MenuItem("Ungenutze Linien löschen");
        clearLines.setOnAction((ActionEvent event) -> {
            ServerRequest r = new ServerRequest("command", new HashMap<String, String>() {
                {
                    put("command", "clearroutes");
                }
            });
            JsonObject o = r.getAsJsonObject();
            if (o.get("result").getAsString().equalsIgnoreCase("success")) {
                Alert a = new Alert(Alert.AlertType.INFORMATION);
                a.setTitle("Befehl ausführen...");
                a.setHeaderText(o.get("message").getAsString());
                a.showAndWait();
            } else {
                Alert a = new Alert(Alert.AlertType.ERROR);
                a.setTitle("Befehl ausführen...");
                a.setHeaderText(o.get("message").getAsString());
                a.showAndWait();
            }
        });
        MenuItem serverRefresh = new MenuItem("Daten neu laden");
        serverRefresh.setAccelerator(new KeyCodeCombination(KeyCode.R, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.DOWN));
        serverRefresh.setOnAction((ActionEvent event) -> {
            m.loadData();
            loadStationView();
        });
        server.getItems().add(serverRefresh);
        MenuItem serverCommand = new MenuItem("Befehl ausführen...");
        serverCommand.setOnAction((ActionEvent event) -> {
            TextInputDialog text = new TextInputDialog();
            text.setTitle("Befehl ausführen...");
            text.setHeaderText("Bitte geben Sie den Befehl ein");
            Optional<String> result = text.showAndWait();
            if (result.isPresent() && !result.get().isEmpty()) {
                ServerRequest r = new ServerRequest("command", new HashMap<String, String>() {
                    {
                        put("command", result.get());
                    }
                });
                JsonObject o = r.getAsJsonObject();
                if (o.get("result").getAsString().equalsIgnoreCase("success")) {
                    Alert a = new Alert(Alert.AlertType.INFORMATION);
                    a.setTitle("Befehl ausführen...");
                    a.setHeaderText(o.get("message").getAsString());
                    a.showAndWait();
                } else {
                    Alert a = new Alert(Alert.AlertType.ERROR);
                    a.setTitle("Befehl ausführen...");
                    a.setHeaderText(o.get("message").getAsString());
                    a.showAndWait();
                }
            }
        });
        server.getItems().add(serverCommand);
        MenuItem save = new MenuItem("Stationen Speichern");
        MenuItem add = new MenuItem("Neue Linie");
        save.setOnAction((ActionEvent event) -> {
            for (Entry<String, Change> entry : m.changes.entrySet()) {
                entry.getValue().onSave();
            }
            m.changes.clear();
        });
        add.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.DOWN));
        add.setOnAction((ActionEvent event) -> {
            // Create the custom dialog.
            Dialog<Result> dialog = new Dialog<>();
            dialog.setTitle("Linie hinzufügen");
            dialog.setHeaderText("Linie hinzufügen");

// Set the button types.
            ButtonType saveButtonType = new ButtonType("Speichern", ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(saveButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            TextField name = new TextField();
            name.setPromptText("Name");
            //TextField farbe = new TextField();
            ColorPicker farbe = new ColorPicker();
            farbe.setPromptText("Farbe");
            TextField zeit = new TextField();
            zeit.setPromptText("Rhythmus");
            ChoiceBox typ = new ChoiceBox();
            typ.getItems().add("Zug");
            typ.getItems().add("S-Bahn");
            typ.getItems().add("U-Bahn");
            typ.getItems().add("Bus");
            typ.getItems().add("Ruderboot");

            grid.add(new Label("Name:"), 0, 0);
            grid.add(name, 1, 0);
            grid.add(new Label("Typ:"), 0, 1);
            grid.add(typ, 1, 1);
            grid.add(new Label("Rhythmus (in min):"), 0, 2);
            grid.add(zeit, 1, 2);
            grid.add(new Label("Farbe:"), 0, 3);
            grid.add(farbe, 1, 3);

            dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
            Platform.runLater(() -> name.requestFocus());

            dialog.setResultConverter(button -> {
                if (button.getButtonData() == ButtonData.OK_DONE) {
                    String c = Integer.toHexString(farbe.getValue().hashCode()).substring(0, 6);
                    return new Result(name.getText(), (String) typ.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(zeit.getText()), c);
                } else {
                    return null;
                }
            });

            dialog.showAndWait().ifPresent(result -> {
                // host01.betafase.com:1999/map/add_line?name=++&type=++&color=++&time=++
                JsonObject jo = new ServerRequest("map/add_line", new HashMap<String, String>() {
                    {
                        put("name", result.getName());
                        put("type", result.getTyp());
                        put("time", Integer.toString(result.getZeit()));
                        put("color", result.getFarbe());
                    }
                }).getAsJsonObject();
                if (jo.get("result").getAsString().equalsIgnoreCase("success")) {
                    Alert a = new Alert(Alert.AlertType.INFORMATION);
                    a.setTitle("Linie hinzufügen");
                    a.setHeaderText(jo.get("message").getAsString());
                    a.showAndWait();
                    m.loadData();
                    loadStationView();
                } else {
                    Alert a = new Alert(Alert.AlertType.ERROR);
                    a.setTitle("Linie hinzufügen");
                    a.setHeaderText("Die Linie konnte nicht hinzugefügt werden.");
                    a.setContentText(jo.get("message").getAsString());
                    a.showAndWait();
                }
            });
        });
        save.setAccelerator(new KeyCodeCombination(KeyCode.S, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.ANY, KeyCombination.ModifierValue.DOWN));
        line.getItems().add(add);
        line.getItems().add(editLines);
        line.getItems().add(clearLines);
        data.getItems().add(save);
        b.getMenus().add(data);
        b.getMenus().add(server);
        b.getMenus().add(line);
        getChildren().add(b);

        m = new ManageStationView(this);
        getChildren().add(m);
        widthProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            load();
        });
        heightProperty().addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
            load();
        });
    }

    public void load() {
        loadStationView();
    }

    public void loadStationView() {
        m.setLayoutX(20);
        m.setLayoutY(40);
        m.setWidth(this.getWidth() - 40);
        m.setHeight(this.getHeight() - 80);
        m.refreshCoords();
        m.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    public static class Result {

        String name;
        String typ;
        int zeit;
        String farbe;

        public Result(String name, String typ, int zeit, String farbe) {
            this.name = name;
            this.typ = typ;
            this.zeit = zeit;
            this.farbe = farbe;
        }

        public String getName() {
            return name;
        }

        public String getTyp() {
            return typ;
        }

        public int getZeit() {
            return zeit;
        }

        public String getFarbe() {
            return farbe;
        }
    }

}
