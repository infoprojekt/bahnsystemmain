/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.manage;

import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author dbrosch
 */
public class TicketCheckController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private TextField ticketID;

    @FXML
    private Label ticketInfo;

    @FXML
    private Label ticketImage;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        reset();
        ticketID.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.isEmpty()) {
                try {
                    if (newValue.length() > 6) {
                        ticketID.textProperty().setValue(oldValue);
                        return;
                    }
                    int i = Integer.parseInt(newValue);
                    if (newValue.length() == 6) {
                        validate(i);
                    } else if (!ticketImage.getText().isEmpty()) {
                        reset();
                    }
                } catch (NumberFormatException ex) {
                    ticketID.textProperty().setValue(oldValue);
                    if (!ticketImage.getText().isEmpty()) {
                        reset();
                    }
                }
            }
        });
    }

    public void reset() {
        root.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        //ticketID.setText("");
        ticketInfo.setText("Bitte geben Sie den 6-stelligen Code ein");
        ticketImage.setText("");
    }

    public void validate(int token) {
        ServerRequest r = new ServerRequest("ticket/check", new HashMap<String, String>() {
            {
                put("token", Integer.toString(token));
            }
        });
        JsonObject o = r.getAsJsonObject();
        if (o.get("result").getAsString().equalsIgnoreCase("success")) {
            ticketImage.setText("✔");
            ticketInfo.setText(o.get("message").getAsString());
            root.setBackground(new Background(new BackgroundFill(Color.LIGHTGREEN, CornerRadii.EMPTY, Insets.EMPTY)));
        } else {
            ticketImage.setText("✘");
            ticketInfo.setText(o.get("message").getAsString());
            root.setBackground(new Background(new BackgroundFill(Color.LIGHTCORAL, CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }

}
