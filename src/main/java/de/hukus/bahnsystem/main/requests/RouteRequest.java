/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* Diese Funktion holt die Routen vom Webserver */
package de.hukus.bahnsystem.main.requests;

import de.hukus.bahnsystem.main.auth.User;
import java.util.HashMap;

/**
 *
 * @author Hosl.Vitus
 */
public class RouteRequest extends ServerRequest {

    public RouteRequest(final String from, final String to, final boolean sbahn, final boolean ubahn, final boolean bus, final boolean zug, final boolean ruderboot) {
        super("route", new HashMap<String, String>() {
            {
                put("from", from);
                put("to", to);
                put("sbahn", Boolean.toString(sbahn));
                put("ubahn", Boolean.toString(ubahn));
                put("bus", Boolean.toString(bus));
                put("zug", Boolean.toString(zug));
                put("ruderboot", Boolean.toString(ruderboot));
            }
        });
    }

}
