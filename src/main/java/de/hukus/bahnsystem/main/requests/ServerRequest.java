/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.requests;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import de.hukus.bahnsystem.main.auth.User;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is for educational purposes only. Copying is only allowed for
 * non-commercial purposes. For more information visit skool-is-cool.com
 *
 * @author Brosch.Dominik
 */
public class ServerRequest {

    private String path;
    private String result;
    private final String body;

    /**
     * Starts a new Server Request without parameters
     *
     * @param path The path for the request (without trailing /)
     * @deprecated Should only be used for requests without params.
     */
    @Deprecated
    public ServerRequest(String path) {
        this.path = path;
        result = requestFromServer(User.user);
        this.body = null;
    }

    /**
     * Starts a new Server Request with given parameters as a post request.
     *
     * @param path The path for the request (without trailing /)
     * @param params The params for the request.
     * @param body The Request body for post requests.
     */
    public ServerRequest(String path, Map<String, String> params, String body) {
        if (!params.isEmpty()) {
            path = path + "?";
            for (Entry<String, String> entries : params.entrySet()) {
                if (!path.endsWith("?")) {
                    path = path + "&";
                }
                try {
                    path = path + entries.getKey() + "=" + URLEncoder.encode(entries.getValue(), "UTF-8");
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(ServerRequest.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        this.path = path;
        System.out.println("Path: " + path);
        this.body = body;
        result = requestFromServer(User.user);
        System.out.println("Result: " + result);
    }

    /**
     * Starts a new Server Request with given parameters.
     *
     * @param path The path for the request (without trailing /)
     * @param params The params for the request.
     */
    public ServerRequest(String path, Map<String, String> params) {
        this(path, params, null);
    }

    /**
     * Converts the raw String to a JsonElement.
     *
     * @return The converted JsonElement
     */
    public JsonElement getAsJsonElement() {
        if (result == null) {
            JsonObject o = new JsonObject();
            o.addProperty("result", "error");
            o.addProperty("description", "Connection failed!");
            return o;
        }
        return new JsonParser().parse(result);
    }

    /**
     * Converts the raw String to a JsonObject
     *
     * @return The converted JsonObject
     */
    public JsonObject getAsJsonObject() {
        if (result == null) {
            JsonObject o = new JsonObject();
            o.addProperty("result", "error");
            o.addProperty("description", "Connection failed!");
            return o;
        }
        JsonObject o = getAsJsonElement().getAsJsonObject();
        if (!o.get("result").getAsString().equalsIgnoreCase("success")) {
            System.out.println("An error occured: " + o.get("description").getAsString());
        }
        return o;
    }

    /**
     * Returns the raw request result
     *
     * @return The raw request result
     */
    public String getAsString() {
        return result;
    }

    private String requestFromServer(User u) {
        try {
            URL bf = new URL("http://host01.betafase.com:1999/" + path);
            HttpURLConnection con = (HttpURLConnection) bf.openConnection();

            con.setDoInput(true);
            if (body != null) {
                con.setDoOutput(true);
                con.setRequestMethod("POST");
            }

            String name = u.getUsername();
            String password = u.getPassHash();

            con.setRequestProperty("Authorization", "Basic " + Base64.getEncoder().encodeToString((name + ":" + password).getBytes()));

            if (body != null) {
                BufferedWriter w = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
                w.write(body);
                w.flush();
            }
            int code = con.getResponseCode();
            switch (code) {
                case 200:
                    BufferedReader r = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                    String line = null;
                    StringBuilder b = new StringBuilder();
                    while ((line = r.readLine()) != null) {
                        b.append(line);
                    }
                    return b.toString();
                case 401: {
                    JsonObject o = new JsonObject();
                    o.addProperty("result", "error");
                    o.addProperty("code", code);
                    o.addProperty("description", "Authentication failed!");
                    return o.toString();
                }
                default: {
                    JsonObject o = new JsonObject();
                    o.addProperty("result", "error");
                    o.addProperty("code", code);
                    o.addProperty("description", con.getResponseMessage());
                    return o.toString();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ServerRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
