/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

/**
 *
 * @author dbrosch
 */
public class Coords2D<X, Y> {

    private X x;
    private Y y;

    public Coords2D(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public Coords2D() {
    }

    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }

    public void setY(Y y) {
        this.y = y;
    }

    public Y getY() {
        return y;
    }

}
