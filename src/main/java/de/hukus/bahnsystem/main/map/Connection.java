/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

import de.hukus.bahnsystem.main.manage.InfoUpdateHandler;
import de.hukus.bahnsystem.main.manage.ManageStationView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author dbrosch
 */
public class Connection extends javafx.scene.shape.Line implements Highlightable {

    int id;
    int start;
    int end;
    int length;
    Line line;
    int stroke = 8;
    boolean highlighted;

    public Connection(int id, int start, int end, int length, Line line) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.length = length;
        this.line = line;
        setHighlighted(false);
    }

    public void setStroke(int stroke) {
        this.stroke = stroke;
        setHighlighted(false);
    }

    public int getStartID() {
        return start;
    }

    public int getEndID() {
        return end;
    }

    public int getLength() {
        return length;
    }

    public Line getLine() {
        return line;
    }

    @Override
    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
        if (highlighted) {
            setStrokeWidth(14);
        } else {
            setStrokeWidth(stroke);
            setStroke(Color.web(line.getColor()));
        }
    }

    @Override
    public void setHighlightType(Station.HighlightType t) {
        if (t == Station.HighlightType.ROUTE) {
            //setStroke(Color.SKYBLUE);

        } else if (t == Station.HighlightType.DEFAULT) {
            setHighlighted(false);
        }
    }

    @Override
    public boolean isHighlighted() {
        return highlighted;
    }

    public void enableEditing(InfoUpdateHandler handler) {
        ContextMenu m = new ContextMenu();
        MenuItem remove = new MenuItem("Entfernen");
        remove.disableProperty().setValue(handler.canRemove(this));
        remove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handler.handleConnectionRemove(id);
            }
        });
        m.getItems().add(remove);
        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (ManageStationView.addCon != null) {
                    return;
                }
                if (!m.isShowing() && event.isSecondaryButtonDown()) {
                    System.out.println("Opening connection menu");
                    m.show(Connection.this, event.getScreenX(), event.getSceneY());
                    event.consume();
                } else if (m.isShowing()) {
                    m.hide();
                }
            }
        });
        setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setHighlighted(true);
                event.consume();
            }
        });
        setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setHighlighted(false);
                event.consume();
            }
        });
    }

}
