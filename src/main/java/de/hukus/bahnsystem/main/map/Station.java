/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

/**
 *
 * @author dbrosch
 */
import de.hukus.bahnsystem.main.manage.InfoUpdateHandler;
import de.hukus.bahnsystem.main.manage.ManageStationView;
import de.hukus.bahnsystem.main.manage.NewCon;
import de.hukus.bahnsystem.main.user.MapInteractionHandler;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class Station extends StackPane implements Highlightable {

    private String name;
    private int id;
    public Circle c;
    private int def_x;
    private int def_y;
    private boolean highlighted;

    public Station(int def_x, int def_y, String name, int id) {
        this.def_x = def_x;
        this.def_y = def_y;
        c = new Circle();
        setHighlighted(false);
        getChildren().add(c);
        Text t = new Text(name);
        getChildren().add(t);
        setName(name);
        setID(id);
    }

    @Override
    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
        if (highlighted) {
            c.setRadius(14);
            c.setFill(Color.WHITE);
            c.setStroke(Color.DARKRED);
            c.setStrokeWidth(4);
        } else {
            c.setRadius(14);
            c.setFill(Color.WHITE);
            c.setStroke(Color.GRAY);
            c.setStrokeWidth(2);
        }
    }

    @Override
    public void setHighlightType(HighlightType t) {
        switch (t) {
            case GREEN:
                c.setStroke(Color.GREENYELLOW);
                break;
            case RED:
                c.setStroke(Color.DARKRED);
                break;
            case ROUTE:
                c.setStroke(Color.SKYBLUE);
            default:
                c.setStroke(Color.GRAY);
        }
    }

    @Override
    public boolean isHighlighted() {
        return highlighted;
    }

    public void setPosition(double x, double y) {
        setLayoutX(x);
        setLayoutY(y);
    }

    public int getX() {
        return def_x;
    }

    public int getY() {
        return def_y;
    }

    public Coords2D<Double, Double> getPosition() {
        return new Coords2D<>(getLayoutX(), getLayoutY());
    }

    public String getName() {
        return name;
    }

    public DoubleBinding getCenterX() {
        return this.layoutXProperty().add(this.widthProperty().divide(2));
    }

    public DoubleBinding getCenterY() {
        return this.layoutYProperty().add(this.heightProperty().divide(2));
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public void enableSelecting(MapInteractionHandler handler) {
        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                handler.onSelect(Station.this);
            }
        });
    }

    public void enableEditing(InfoUpdateHandler handler) {
        ContextMenu m = new ContextMenu();
        MenuItem mi_add = new MenuItem("Verbindung hinzufügen");
        MenuItem mi_remove = new MenuItem("Entfernen");
        mi_add.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                NewCon c = new NewCon(Station.this);
                handler.handleConnectionAddStart(c);
            }
        });
        mi_remove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                handler.handleStationRemove(Station.this);
            }
        });

        m.getItems().add(mi_add);
        m.getItems().add(mi_remove);
        final Delta dragDelta = new Delta();
        final Coords2D<Double, Double> startcoords = new Coords2D<>();
        setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (ManageStationView.addCon != null) {
                    NewCon ac = ManageStationView.addCon;
                    if (ac.getStartID() != getID()) {
                        handler.handleConnectionAddCompletion(ac.getStartID(), getID());
                    }
                    return;
                }
                if (!m.isShowing() && mouseEvent.isSecondaryButtonDown()) {
                    m.show(Station.this, mouseEvent.getScreenX(), mouseEvent.getScreenY());
                    mouseEvent.consume();
                    return;
                } else if (m.isShowing()) {
                    m.hide();
                }
                if (mouseEvent.isPrimaryButtonDown() && getID() != 0) {
                    // record a delta distance for the drag and drop operation.
                    startcoords.setX(getLayoutX());
                    startcoords.setY(getLayoutY());
                    dragDelta.x = getLayoutX() - mouseEvent.getSceneX();
                    dragDelta.y = getLayoutY() - mouseEvent.getSceneY();
                    setCursor(Cursor.MOVE);
                }
            }
        });
        setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.isPrimaryButtonDown() && getID() != 0) {
                    setCursor(Cursor.HAND);
                }
            }
        });
        setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (mouseEvent.isPrimaryButtonDown() && getID() != 0) {
                    double newX = mouseEvent.getSceneX() + dragDelta.x;
                    double newY = mouseEvent.getSceneY() + dragDelta.y;
                    if (newX < 0 || newY < 0) {
                        return;
                    }
                    setLayoutX(newX);
                    setLayoutY(newY);
                    handler.handleStationDragCompletion(Station.this, startcoords);
                }
            }
        });
        setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                setCursor(Cursor.HAND);
                setHighlighted(true);
                if (ManageStationView.addCon != null && ManageStationView.addCon.getStartID() != getID()) {
                    setHighlightType(HighlightType.GREEN);
                }
            }
        });

        setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                setHighlighted(false);
            }
        });
    }

    public static enum HighlightType {
        GREEN, RED, ROUTE, DEFAULT;
    }

    // records relative x and y co-ordinates.
    private class Delta {

        double x, y;
    }

}
