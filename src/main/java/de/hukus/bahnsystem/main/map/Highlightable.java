/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

/**
 *
 * @author Brosch.Dominik
 */
public interface Highlightable {
    
    
    public void setHighlighted(boolean highlighted);
    
    public boolean isHighlighted();
    
    public void setHighlightType(Station.HighlightType t);
}
