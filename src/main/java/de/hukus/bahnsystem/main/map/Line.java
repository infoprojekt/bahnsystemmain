/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

/**
 *
 * @author Brandhuber.Fritz
 */
public class Line {

    int id;
    String name;
    String type;
    int time;
    int start;
    int end;
    String color;

    public Line(int id, String name, String type, int time, int start, int end, String color) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.time = time;
        this.start = start;
        this.end = end;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getTime() {
        return time;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return getName() + " (" + getType() + ")";
    }

}
