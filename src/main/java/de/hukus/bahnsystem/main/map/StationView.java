/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.hukus.bahnsystem.main.map;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import de.hukus.bahnsystem.main.requests.ServerRequest;
import java.util.Iterator;
import java.util.LinkedList;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;

/**
 *
 * @author dbrosch und Fritz.Brandhuber
 */
//Server: host01.betafase.com:1999/map/view
//user test pw geheim
public class StationView extends AnchorPane {

    JsonObject jo;
    int pref_minX;
    int pref_minY;
    int pref_maxX;
    int pref_maxY;
    int pref_x;
    int pref_y;
    protected Station[] stations;
    protected Connection[] connections;
    protected Line[] lines;
    Coords2D<Double, Double> center;
    double Xfactor;
    double Yfactor;

    public StationView() {

        DropShadow ds = new DropShadow();
        ds.setOffsetY(3.0);
        ds.setColor(Color.color(0.4, 0.4, 0.4));
        setEffect(ds);

        getStyleClass().add("map");
        setWidth(560);
        setHeight(500);
        System.out.println("Loading data...");

        loadData();

        System.out.println("Data loaded.");
    }

    public Station[] getStation() {
        return stations;
    }

    public int getStationIDfromName(String name) {
        for (int i = 0; i < stations.length; i++) {
            if (name.equalsIgnoreCase(stations[i].getName())) {
                return i;
            }
        }
        return -1;
    }

    public Connection[] getConnection() {
        return connections;
    }

    public Line[] getLine() {
        return lines;
    }

    @Override
    public void setWidth(double value) {
        super.setWidth(value); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setHeight(double value) {
        super.setHeight(value); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Updates the coords of all stations and connections. Should be used
     * onResize.
     *
     */
    public void refreshCoords() {
        double x_size = getWidth();
        double y_size = getHeight();
//        System.out.println("[" + new SimpleDateFormat("HH:mm:ss").format(new Date()) + "] Refreshing for " + x_size + " & " + y_size);
        center = getCenter(x_size, y_size);
        double centerX = center.getX();
        double centerY = center.getY();
        if (Math.abs(pref_minX) < pref_maxX) {
//            System.out.println("PrefMaxX: " + pref_maxX);
            Xfactor = ((x_size / 2) - 20) / (pref_maxX);
        } else {
//            System.out.println("PrefMinX: " + pref_minY);
            Xfactor = ((x_size / 2) - 20) / (Math.abs(pref_minX));
        }
        if (Math.abs(pref_minY) < pref_maxY) {
//            System.out.println("PrefMaxY: " + pref_maxY);
            Yfactor = -1 * ((y_size / 2) - 20) / (pref_maxY);
        } else {
//            System.out.println("PrefMinY: " + pref_minY);
            Yfactor = -1 * ((y_size / 2) - 20) / (Math.abs(pref_minY));
        }
//        System.out.println("PrefMinX (not shown): " + pref_minX);
//        System.out.println("PrefMaxX (not shown):" + pref_maxX);
//        System.out.println("X-Factor: " + Xfactor + " Y-Factor: " + Yfactor);
        for (Station s : stations) {
            s.setPosition(s.getX() * Xfactor + center.getX() - 7, s.getY() * Yfactor + center.getY() - 7);
        }

    }

    public Coords2D<Integer, Integer> getCoords(double x, double y) {
        int x_mod = (int) ((x - center.getX() + 7) / Xfactor);
        int y_mod = (int) ((y - center.getY() + 7) / Yfactor);
        return new Coords2D<>((int) x_mod, (int) y_mod);
    }

    public Coords2D<Double, Double> getCenter(double x_size, double y_size) {
        return new Coords2D<>(x_size / 2, y_size / 2);
    }

    public void loadData() {
        jo = new ServerRequest("map/view").getAsJsonObject();
        JsonObject bounds = jo.get("bounds").getAsJsonObject();
        pref_minX = bounds.get("minX").getAsInt();
        pref_minY = bounds.get("minY").getAsInt();
        pref_maxX = bounds.get("maxX").getAsInt();
        pref_maxY = bounds.get("maxY").getAsInt();
        pref_x = bounds.get("x").getAsInt();
        pref_y = bounds.get("y").getAsInt();

        this.getChildren().clear();

        //Stationen
        JsonArray json_stations = jo.get("stations").getAsJsonArray();
        stations = new Station[jo.get("stations").getAsJsonArray().size()];
        for (JsonElement e : json_stations) {
            JsonObject json_station = e.getAsJsonObject();
            String name = json_station.get("name").getAsString();
            int id = json_station.get("id").getAsInt();
            //double x = json_station.get("x").getAsInt() * Xfactor + centerX;
            //double y = json_station.get("y").getAsInt() * Yfactor + centerY;
            //Add line
            stations[id] = new Station(json_station.get("x").getAsInt(), json_station.get("y").getAsInt(), name, id);
            this.getChildren().add(stations[id]);
        }

        //Linien
        JsonArray json_lines = jo.get("lines").getAsJsonArray();
        lines = new Line[jo.get("lines").getAsJsonArray().size()];

        for (JsonElement e : json_lines) {
            JsonObject lin = e.getAsJsonObject();
            int id = lin.get("id").getAsInt();
            String name = lin.get("name").getAsString();
            String type = lin.get("type").getAsString();
            int time = lin.get("time").getAsInt();
            int start = lin.get("start").getAsInt();
            int end = lin.get("end").getAsInt();
            String color = lin.get("color").getAsString();
            Line l = new Line(id, name, type, time, start, end, color);
            lines[id] = l;
        }

        // Connections
        JsonArray json_connections = jo.get("connections").getAsJsonArray();
        connections = new Connection[jo.get("connections").getAsJsonArray().size()];

        // Process connections from the server
        for (JsonElement e : json_connections) {
            JsonObject con = e.getAsJsonObject();
            int start = con.get("start").getAsInt();
            int end = con.get("end").getAsInt();
            int id = con.get("id").getAsInt();
            int length = con.get("length").getAsInt();
            int line = con.get("line").getAsInt();

            Connection c = new Connection(id, start, end, length, getLine()[line]);
            LinkedList<Connection> toAdd = new LinkedList<>();
            for (Connection c0 : connections) {
                if (c0 != null && ((c0.getStartID() == start && c0.getEndID() == end) || (c0.getStartID() == end && c0.getEndID() == start))) {
                    toAdd.add(c0);
                }
            }
            System.out.println("Verbindungen zwischen " + stations[start].getName() + " und " + stations[end].getName() + ": " + (toAdd.size() + 1));
            connections[id] = c;
            this.getChildren().add(c);
            if (toAdd.isEmpty()) {
                c.startXProperty().bind(stations[start].getCenterX());
                c.startYProperty().bind(stations[start].getCenterY());
                c.endXProperty().bind(stations[end].getCenterX());
                c.endYProperty().bind(stations[end].getCenterY());
            } else {
                toAdd.add(c);
                int half = toAdd.size();
                if (half % 2 == 1) {
                    Iterator<Connection> it = toAdd.iterator();
                    for (int i = -1 * (half - 1) / 2; i <= (half - 1) / 2; i++) {
                        if (it.hasNext()) {
                            Connection next = it.next();
                            if (next.startXProperty().isBound()) {
                                next.startXProperty().unbind();
                            }
                            if (next.startYProperty().isBound()) {
                                next.startYProperty().unbind();
                            }
                            if (next.endXProperty().isBound()) {
                                next.endXProperty().unbind();
                            }
                            if (next.endYProperty().isBound()) {
                                next.endYProperty().unbind();
                            }
                            if (i > 0) {
                                next.startXProperty().bind(stations[start].getCenterX().add(i * 5));
                                next.startYProperty().bind(stations[start].getCenterY().add(i * 5));
                                next.endXProperty().bind(stations[end].getCenterX().add(i * 5));
                                next.endYProperty().bind(stations[end].getCenterY().add(i * 5));
                            } else {
                                int i0 = i * -1;
                                next.startXProperty().bind(stations[start].getCenterX().subtract(i0 * 5));
                                next.startYProperty().bind(stations[start].getCenterY().subtract(i0 * 5));
                                next.endXProperty().bind(stations[end].getCenterX().subtract(i0 * 5));
                                next.endYProperty().bind(stations[end].getCenterY().subtract(i0 * 5));
                            }
                            next.setStroke(4);
                        }
                    }
                } else {
                    Iterator<Connection> it = toAdd.iterator();
                    for (int i = -1 * (half) / 2; i < (half) / 2; i++) {
                        if (it.hasNext()) {
                            Connection next = it.next();
                            if (i > 0) {
                                next.startXProperty().bind(stations[start].getCenterX().add(i * 5));
                                next.startYProperty().bind(stations[start].getCenterY().add(i * 5));
                                next.endXProperty().bind(stations[end].getCenterX().add(i * 5));
                                next.endYProperty().bind(stations[end].getCenterY().add(i * 5));
                            } else {
                                int i0 = i * -1;
                                next.startXProperty().bind(stations[start].getCenterX().subtract(i0 * 5));
                                next.startYProperty().bind(stations[start].getCenterY().subtract(i0 * 5));
                                next.endXProperty().bind(stations[end].getCenterX().subtract(i0 * 5));
                                next.endYProperty().bind(stations[end].getCenterY().subtract(i0 * 5));
                            }
                            next.setStroke(4);
                            System.out.println("Iterating through connections. i=" + i);
                        }
                    }
                }
            }
        }

        for (Station s : stations) {
            s.toFront();
        }
        //todo bring lied

    }

}
